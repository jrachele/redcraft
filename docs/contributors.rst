============
Contributors
============
REDCRAFT is directed by its creator *Dr. Homayoun Valafar*

Current contributors
====================
* Caleb Parks
* Julian Rachele
* Jeffrey Russell

Past contributors
=================
* Mike Bryson
* Paul Shealy
* Zach Swearingen
* Mikhail Simin
* Casey Cole
* Earron Twitty